# Hello Spec

| do with | com.greenpepper.demo.fixtures.HelloFixture  |
|---------|-------------|
| accept | meet the person named | wattazoum |
| check | that Hello says | hello wattazoum ! |

